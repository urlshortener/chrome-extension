'use strict';

document.getElementById("urlclick").addEventListener("click", short);
document.getElementById("apiclick").addEventListener("click", saveapi);
document.getElementById("clearapi").addEventListener("click", clearApi);
document.getElementById("customshort").addEventListener("click", customMenu);
document.getElementById("customurlclick").addEventListener("click", customShort);

var apikey = localStorage.getItem('apikey');

if (apikey != null) {
  document.getElementById("short_menu").setAttribute("style","display:block;");
  document.getElementById("setapi").setAttribute("style","display:none;");
  document.getElementById("clearapi").setAttribute("style","display:block;");
}

chrome.tabs.query({active: true,lastFocusedWindow: true}, function(tabs) {
  var tab = tabs[0];
  document.getElementById('urltext').value=tab.url;
});

function short() {
  var long_url = document.getElementById("urltext").value;
  var apikey   = localStorage.getItem('apikey');
  shortUrl(apikey, long_url);
}

function saveapi() {
  var apikey    = document.getElementById("apikey").value;
  var apiserver = document.getElementById("apiserver").value;
  localStorage.setItem('apikey', apikey);
  localStorage.setItem('apiserver', apiserver);
  document.location.reload(true);
}

function clearApi() {
  localStorage.removeItem('apikey');
  document.location.reload(true);
}

function shortUrl(apikey, long_url) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.status == 200 || this.status == 201) {
      document.getElementById("shorturl").value = this.responseText;
      document.getElementById("shorturl").setAttribute("style","display:block;text-align: center;font-weight: bold;");
      document.getElementById("info").innerHTML = "Link is copied to clipboard";
      document.getElementById("customshort").setAttribute("style","display:none;");
      copyToClipboard();
    } 
    if (this.status == 422) {
      document.getElementById("info").innerHTML = "No valid entries";
    }
    if (this.status == 401) {
      document.getElementById("info").innerHTML = "No valid API key";
    }
  }
  var apiserver = localStorage.getItem('apiserver');
  xhttp.open("POST", apiserver+"/shorten", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("long_url="+long_url+"&apikey="+apikey);
}

function customMenu(){
  document.getElementById("customshort").setAttribute("style","display:none;");
  document.getElementById("customMenu").setAttribute("style","display;");
  document.getElementById("urlclick").setAttribute("style","display:none;");
}

function customShort() {

  var long_url = document.getElementById("urltext").value;
  var shorturl = document.getElementById("customshorturl").value;

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.status == 200 || this.status == 201) {
      document.getElementById("customshorturl").value = this.responseText;
      document.getElementById("customshorturl").setAttribute("style","text-align: center;font-weight: bold;");
      document.getElementById("shortBtn").setAttribute("style","display:none;");
      document.getElementById("error").setAttribute("style","display:none;");
      document.getElementById("info").innerHTML = "Link is copied to clipboard";
      copyCutomToClipboard();
    } 
    if (this.status == 401) {
      document.getElementById("info").innerHTML = "No valid API key";
    }
    if (this.status == 422) {
      document.getElementById("info").innerHTML = "No valid entries";
    }
    if (this.status == 423) {
      document.getElementById("error").innerHTML = "This shortname isn't available anymore.";
    }
    if (this.status == 424) {
      document.getElementById("error").innerHTML = "Only alphanumeric character allowed.";
    }

  }
  var apiserver = localStorage.getItem('apiserver');
  xhttp.open("POST", apiserver+"/custom", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("long_url="+long_url+"&apikey="+apikey+"&short="+shorturl);
}

function copyCutomToClipboard() {
  var copyText = document.getElementById("customshorturl");
  copyText.select();
  document.execCommand("copy");
}

function copyToClipboard() {
  var copyText = document.getElementById("shorturl");
  copyText.select();
  document.execCommand("copy");
}